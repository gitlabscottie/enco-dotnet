FROM mcr.microsoft.com/dotnet/sdk:3.1 AS build
WORKDIR /app 

COPY *.sln .
COPY HelloWord.Api/*.csproj ./HelloWord.Api/
COPY HelloWord.Bll/*.csproj ./HelloWord.Bll/
COPY HelloWord.Dal/*.csproj ./HelloWord.Dal/

RUN dotnet restore 

COPY HelloWord.Api/. ./HelloWord.Api/
COPY HelloWord.Bll/. ./HelloWord.Bll/
COPY HelloWord.Dal/. ./HelloWord.Dal/ 

WORKDIR /app/HelloWord.Api
RUN dotnet publish -c Release -o out 

FROM mcr.microsoft.com/dotnet/aspnet:3.1 AS runtime 
WORKDIR /app 

COPY --from=build /app/HelloWord.Api/out ./
ENV ASPNETCORE_URLS http://*:5000
ENTRYPOINT ["dotnet", "HelloWord.Api.dll"] 
